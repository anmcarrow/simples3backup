Just set of tools (one shellscript and conffiles) for compress and upload into S3-storage 
files and dirs from your linux-server

Requiremens: `s3cmd`, `tar`, `pigz` and `split`

#### List of content:
- contributors.txt — list of contributors
- exclude-home.list — list of exceptions from homedir arch
- exclude-root.list — list of exceptions from rootfs arch
- README.md — this file
- s3backup.sh
- s3.cfg.sample
