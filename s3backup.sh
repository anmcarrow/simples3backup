#!/bin/bash

# Just a simple script for batch archiving rootfs (system files), homedir/websites
# (without maildirs), maildirs in /home/vmail and /var/lib/mysql in separately TAR+BZIP files,
# with stream (without saving locally) upload all files in remote S3-storage

# Requirements: This script use `pigz` (as multithread gzip-algo copressor) and `split` (as archive splitting utility)
# also decompress splitting archive syntax is "cat archive.tar.gz_ | tar -C / -xvf -"
# project by Stanislavvv (https://github.com/stanislavvv/stdin2s3) as S3 uploader.

# 2015, BSDL, am_ (mailbox@anmcarrow.me)


WORKDIR="/usr/scripts/s3backup"

S3CFG="$WORKDIR/s3.cfg"
BUCKET="fullbackup-${HOSTNAME}"

EXCLUDE1="$WORKDIR/exclude-root.list"
EXCLUDE2="$WORKDIR/exclude-home.list"

#S3UPLOADER="/usr/scripts/s3backup/stdin2s3/stdin2s3"

TARGET1="/"
TARGET2="/home"
TARGET3="/home/vmail"
TARGET4="/var/lib/mysql"

# be gently

renice -n 10 -p $$

# prepare

mkdir /tmp/tempbackups
s3cmd -c $S3CFG mb ${BUCKET}

# creating rootfs archive

#tar -C / -cpvjO ${TARGET1} --exclude-from="${EXCLUDE1}" | $S3UPLOADER -c $WORKDIR/s3.cfg s3://${BUCKET}/rootfs.tar.gz
tar -C / -cpvO / --exclude-from="${EXCLUDE1}" | pigz -c |  split -b 3072MiB - /tmp/tempbackups/rootfs.tar.gz_

# creating sites archive

#tar -C / -cpvjO ${TARGET2} --exclude-from="${EXCLUDE2}" | $S3UPLOADER -c $WORKDIR/s3.cfg s3://${BUCKET}/websites.tar.gz
tar -C / -cpvO ${TARGET2} --exclude-from="${EXCLUDE2}" | pigz -c | split -b 3072MiB - /tmp/tempbackups/home.tar.gz_

# creating mail archive

#tar -C / -cpvjO ${TARGET3} | $S3UPLOADER -c $WORKDIR/s3.cfg s3://${BUCKET}/maildir.tar.gz
tar -C / -cpvO ${TARGET3} | pigz -c | split -b 3072MiB - /tmp/tempbackups/vmail.tar.gz_

# creating mysql archive

#tar -C / -cpvjO ${TARGET4} | $S3UPLOADER -c $WORKDIR/s3.cfg s3://${BUCKET}/mysql.tar.gz
tar -C / -cpvO ${TARGET4} | pigz -c | split -b 3072MiB - /tmp/tempbackups/mysql.tar.gz_

# uploading and cleaning
s3cmd -c $S3CFG put /tmp/tempbackups/* s3://${BUCKET} && rm -rf /tmp/tempbackups

exit 0
